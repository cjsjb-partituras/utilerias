#!/bin/bash

BASEDIR="$(readlink -e "$(dirname "$0")")"

# Generar definición del libro:
"${BASEDIR}/utils/manifest2libro.php" default.xml > libro.def
echo "=========="
cat libro.def
echo "=========="
# Generar el contenido:
"${BASEDIR}/utils/haz-contenido.sh"
# Generar la portada:
"${BASEDIR}/utils/haz-portada.sh"
# Generar el índice:
"${BASEDIR}/utils/haz-indice.sh"

indice_pags="$(pdfinfo indice.pdf | grep ^Pages | awk '{print $2}')"
if [ "$(( indice_pags % 2))" = "1" ]; then
    tras_indice="${BASEDIR}/utils/paginas/hoja-blanca.pdf"
else
    tras_indice=""
fi
gs -q -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -sOutputFile=cjsjb-partituras.pdf \
    portada.pdf \
    "${BASEDIR}/utils/paginas/hoja-blanca.pdf" \
    indice.pdf \
    ${tras_indice} \
    contenido.pdf
