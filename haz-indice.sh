#!/bin/bash

UTILDIR=$(readlink -f $(dirname $0))

INDICE="indice.fodt"
INDOUT=${INDICE}.body

cat << EOF > ${INDOUT}
 <office:body>
  <office:text>
   <text:h text:style-name="Heading_20_1" text:outline-level="1">ÍNDICE</text:h>
EOF

cat indice.txt |
	while read n; do
		PAGE=${n%%,*}
		TITLE=${n#*,}
		# wait, what?
		ISPDF=$(echo $n | cut -d, -f2 | egrep -o '.pdf$')

		if [ "${ISPDF}" ]; then
			TITLE=$(echo $n | cut -d, -f3)
			echo -n '   <text:h text:style-name="Heading_20_2" text:outline-level="2">'	>> ${INDOUT}
			echo -n ${TITLE}	>> ${INDOUT}
			echo -n '<text:tab/>'	>> ${INDOUT}
			echo -n ${PAGE}		>> ${INDOUT}
			echo -n '</text:h>'	>> ${INDOUT}
			echo >> ${INDOUT}
		else
			echo -n '   <text:h text:style-name="Heading_20_3" text:outline-level="3"><text:tab/>'	>> ${INDOUT}
			echo -n ${TITLE}	>> ${INDOUT}
			echo -n '<text:tab/>'	>> ${INDOUT}
			echo -n ${PAGE}		>> ${INDOUT}
			echo -n '</text:h>'	>> ${INDOUT}
			echo >> ${INDOUT}
		fi
	done

cat << EOF >> ${INDOUT}
  </office:text>
 </office:body>
EOF

cat ${UTILDIR}/extras/${INDICE}.head ${INDOUT} ${UTILDIR}/extras/${INDICE}.tail > ${INDICE}
rm ${INDOUT}

LOBIN=$(which libreoffice)
if [ ! -z "${LOBIN}" ]; then
	libreoffice --convert-to pdf:writer_pdf_Export --outdir $(pwd)/ ${INDICE}
else
	echo "Required: libreoffice"
	echo "Please convert ${INDICE} to ${INDICE%%.fodt}.pdf somehow"
	exit 1
fi
