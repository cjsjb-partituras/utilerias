#!/bin/bash

UTILDIR=$(readlink -f $(dirname $0))

CONTRAPORTADA="contraportada.fodt"
INDOUT=${CONTRAPORTADA}.body

cat << EOF > ${INDOUT}
 <office:body>
  <office:text>
   <text:section text:style-name="Sect1" text:name="Section1">
EOF

cat indice.txt |
	while read n; do
		PAGE=${n%%,*}
		TITLE=${n#*,}
		# wait, what?
		ISPDF=$(echo $n | cut -d, -f2 | egrep -o '.pdf$')

		if [ "${ISPDF}" ]; then
			TITLE=$(echo $n | cut -d, -f3)
			echo -n '    <text:h text:style-name="P11" text:outline-level="2">'              >> ${INDOUT}
			echo -n ${TITLE}	>> ${INDOUT}
			echo -n '</text:h>'	>> ${INDOUT}
			echo >> ${INDOUT}
		else
			echo -n '    <text:h text:style-name="P13" text:outline-level="3">'              >> ${INDOUT}
			echo -n ${TITLE}	>> ${INDOUT}
			echo -n '</text:h>'	>> ${INDOUT}
			echo >> ${INDOUT}
		fi
	done

cat << EOF >> ${INDOUT}
   </text:section>
EOF

cat ${UTILDIR}/extras/${CONTRAPORTADA}.head ${INDOUT} ${UTILDIR}/extras/${CONTRAPORTADA}.tail > ${CONTRAPORTADA}
rm ${INDOUT}

LOBIN=$(which libreoffice)
if [ ! -z "${LOBIN}" ]; then
	libreoffice --convert-to pdf:writer_pdf_Export --outdir $(pwd)/ ${CONTRAPORTADA}
else
	echo "Required: libreoffice"
	echo "Please convert ${CONTRAPORTADA} to ${CONTRAPORTADA%%.fodt}.pdf somehow"
	exit 1
fi
